<?php

$newfileArgv = $argv[3];
$newfileStr = explode("=", $newfileArgv);
$desinationFile = $newfileStr[1];
$sourceFile = $argv[2];
$numberOfActiveColumn = 7;

// Right now i kept static and later based on extra argv we can take json and other formats.
$parser = 'csv';

if ($parser == 'csv') {

    try {
        $uniqueRecords = parseCsvFile($sourceFile, $numberOfActiveColumn);
        generateDestinationFile($uniqueRecords, $desinationFile);
    } catch (Exception $e) {
        echo $e->getMessage();
        exit;
    }
}

function generateDestinationFile($uniqueRecords, $desinationFile)
{
    $output = fopen($desinationFile, 'w');
    foreach ($uniqueRecords as $product) {
        fputcsv($output, $product);
    }
    fclose($output) or die("Can't close php://output");
}

// validate each value contains blank or not
function validateData($data)
{
    if (array_search("", $data) !== false) {
        throw new Exception("One of the field values row is empty");
    }
}

function parseCsvFile($sourceFile, $numberOfActiveColumn)
{
    $open = fopen($sourceFile, "r");
    if (!empty($sourceFile)) {
        $totalRec = 0;
        $dataNew = $dataUnique = [];

        while (($data = fgetcsv($open)) !== FALSE) {

            // Read the data but not first one becuse first one is static values
            if ($totalRec > 0) {

                // validate data.
                validateData($data);

                // combine all records into string and covert array to string for easy to check uniqueness and remove extra whitespace
                $strLower = strtolower(implode('', $data));
                $str = str_replace(" ", "", $strLower);

                // dataNew array to check logic and use to count duplicate records
                // dataUnique is final array with count
                if (!in_array($str, $dataNew)) {
                    array_push($dataNew, $str);
                    // first time set count records to ONE
                    $data[$numberOfActiveColumn + 1] = 1;
                    $dataUnique[$str] = $data;
                } else {
                    // Count interation
                    $dataUnique[$str][$numberOfActiveColumn + 1] = $dataUnique[$str][$numberOfActiveColumn + 1] + 1;
                }
            }

            // Add First row as it is.
            if ($totalRec == 0) {
                $data[$numberOfActiveColumn + 1] = 'Count';
                $firstRow[] = $data;
            }
            $totalRec++;

        }
        return array_merge($firstRow, $dataUnique);
    }
}
// Added because we can know that script execution is completed
echo "Completed";